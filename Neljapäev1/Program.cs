﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Neljapäev1
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] arvud = {1, 4, 2, 7, 3, 8, 5, 9};
            //int[] teisedArvud = Enumerable.Range(1, 1000).ToArray();
            //for (int i = 0; i < arvud.Length; i++)
            //{
            //    Console.WriteLine(arvud[i]); //i- le proovi ka ++ ette ja taha panna
            //}

            //{
            //    // sama asja teeb
            //    foreach (var x in arvud)
            //        Console.WriteLine(x);

            //Console.WriteLine("Ütle üks arv: "); //see peaks tsüklis olema
            //int arv = int.Parse(Console.ReadLine());

            //    while (arv < 100) 
            //    {
            //        Console.WriteLine("Tee edasi...");
            //    }
            //    else Console.WriteLine("..."); // see nüüd teeb lõpmatuseni seda - vaata kodus kuidas lõpetaks ära

            string failinimi = @"..\..\nimekiri.txt";

            var loetudread = File.ReadLines(failinimi);

            //Console.WriteLine(loetudread); //lõpuks luges andmed failist fail asub Solution Exploreris programmiga samas kaustas
            //vaata edasi neljapäeva peale lõunast salvestust + Hennu repo

            Dictionary<string, DateTime> nimekiri = new Dictionary<string, DateTime>();

            foreach (var rida in loetudread)
            {
                var osad = rida.Split(',');
                string ik = osad[1];
                string nimi = osad[0];

                //int päev = int.Parse(ik.Substring(5, 2));
                //int kuu = int.Parse(ik.Substring(2, 2));
                //int aasta = int.Parse(ik.Substring(1, 2));
                nimekiri.Add(
                    nimi,
                    new DateTime(
                        (ik[0] - '1') / 2 * 100 + 1800 + int.Parse(ik.Substring(1, 2)),
                        int.Parse(ik.Substring(3, 2)),
                        int.Parse(ik.Substring(5, 2))
                        ))
                        ;
                    



            }



            foreach (var x in nimekiri)

                //Console.WriteLine($"{x.Key} on sündinud {x.Value} ja ta on {(DateTime.Now - x.Value).Days} päeva siin elanud");


                Console.WriteLine($"{x.Key} on {(DateTime.Today - x.Value).Days} aastat vana"); // vaata kodus, kuidas siia aastad saada



        }
    }
}
